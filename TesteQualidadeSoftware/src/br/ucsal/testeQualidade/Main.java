package br.ucsal.testeQualidade;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		 Calcular calcular = new Calcular();
	        Scanner sc = new Scanner(System.in);
	        System.out.println("INSIRA UM VALOR ENTRE 0 E 100");
	        int a = sc.nextInt();
	        if (calcular.verificarNumero(a)) {
	            System.out.println(calcular.fatorial(a));
	        } else {
	            System.out.println("NUMERO INV�LIDO INSERIDO!");
	        }
	        sc.close();
	}

}
