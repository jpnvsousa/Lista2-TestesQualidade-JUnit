
	package br.ucsal.testeQualidade.teste;

	import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.ucsal.testeQualidade.Calcular;

	public class CalcularTeste {
		
		
		Calcular calcular;
		//TENTATIVA DE BOTAR MULTIPLOS TESTES SIMPLIFICADOS
		@Before
		public void setUp() throws Exception {
			calcular = new Calcular();
		}

		//COLOCANDO VARIOS METODOS DE TESTE, AP�S N�O CONSEGUIR COLOCAR DE FORMA SIMPLIFICADA
		@Test
	    public void fatorialDe5() {
	        Calcular calcular = new Calcular();
	        int resultado = 120;
	        assertEquals(resultado, calcular.fatorial(5), 2);
	    }
		
		@Test
		public void fatorialDe6() {
			Calcular calcular = new Calcular();
	        int resultado = 720;
	        assertEquals(resultado, calcular.fatorial(6));
		}
		
		@Test
		public void fatorialDe4() {
			Calcular calcular = new Calcular();
	        int resultado = 24;
	        assertEquals(resultado, calcular.fatorial(4));
		}
		
		@Test
		public void fatorialDe3() {
			Calcular calcular = new Calcular();
	        int resultado = 6;
	        assertEquals(resultado, calcular.fatorial(3));
		}
		
		@Test
		public void fatorialDe7() {
			Calcular calcular = new Calcular();
	        int resultado = 5040;
	        assertEquals(resultado, calcular.fatorial(7));
		}
		
		@Test
		public void fatorialDe1() {
			Calcular calcular = new Calcular();
	        int resultado = 1;
	        assertEquals(resultado, calcular.fatorial(1));
		}
		
		@Test
		public void fatorialDe2() {
			Calcular calcular = new Calcular();
	        int resultado = 2;
	        assertEquals(resultado, calcular.fatorial(2));
		}
		
		@Test
		public void fatorialDe8() {
			Calcular calcular = new Calcular();
	        int resultado = 40320;
	        assertEquals(resultado, calcular.fatorial(8));
		}
		
		@Test
		public void fatorialDe9() {
			Calcular calcular = new Calcular();
	        int resultado = 362880;
	        assertEquals(resultado, calcular.fatorial(9));
		}
		
		@Test
		public void fatorialDe10() {
			Calcular calcular = new Calcular();
	        int resultado = 3628800;
	        assertEquals(resultado, calcular.fatorial(10));
		}

		@Test
		public void verificarNumero() {
			Calcular calcular = new Calcular();
	        boolean resultado = true;
	        assertEquals(resultado, calcular.verificarNumero(5));
		}
		
		@Test
		public void verificarNumero2() {
			Calcular calcular = new Calcular();
	        boolean resultado = false;
	        assertEquals(resultado, calcular.verificarNumero(101));
		}
		
		@Test
		public void verificarNumero3() {
			Calcular calcular = new Calcular();
	        boolean resultado = true;
	        assertEquals(resultado, calcular.verificarNumero(99));
		}
	
	}

